# Camunda diagram

Микросервис создан для отображения диаграммы bpmn по businessKey. 
Микросервис проксирует вызовы к Camunda rest api (для правильной работы CORS) и используя библиотеке bpmn-js отображает диаграмму.

### Как использовать сервис:

1. В файле application.yml необходимо прописать действующий адрес Camunda с открытым REST Api, например:
```yml
camunda:
  base-url: http://localhost:8085/rest
```

2. Производим стандартную сборку maven для приложения spring boot. 
```sh
mvn clean package
```

3. Запускаем приложение
```sh
java -jar ./target/camunda-diagram-0.0.1-SNAPSHOT.jar
```

4. Открываем url в строке браузера
```http request
http://localhost:8080/diagram?businessKey=123
```
Видим набор диаграм, полученных по businessKey.


### Как работает сервис:

1. С помощью rest api Camunda можно получить список активных бизнес-процессов по businessKey. 

```http request
get /process-instance?businessKey={businessKey}
```
```json
[ 
  {
    links: [ ],
    id: "123456",
    definitionId: "someProcess:00:123456",
    businessKey: "123",
    caseInstanceId: null,
    ended: false,
    suspended: false,
    tenantId: null
  }
]
```
2. Используя definitionId можно сделать запрос и получить xml процесса

```http request
get /process-definition/{definitionId}/xml
```
```json
{
  id: "someProcess:00:123456",
  bpmn20Xml: "<xml ... " 
}
```

3. Полученный xml используется библиотекой bpmn-js для отображения диаграммы

4. Чтобы подсветить нужное место в процессе, требуется знать id задачи. Для этого производим запрос по acitivity-instance,
используя processInstanceId (id полученный в первом запросе).

```http request
get //process-instance/{processInstanceId}/activity-instances
```
```json
{
  id: "157202",
  parentActivityInstanceId: null,
  activityId: "someProcess:00:123456",
  activityType: "processDefinition",
  processInstanceId: "123456",
  processDefinitionId: "someProcess:00:123456",
  childActivityInstances: [
    {
      id: "IntermediateCatchEvent_0uyzct1:157224",
      parentActivityInstanceId: "123456",
      activityId: "IntermediateCatchEvent_0uyzct1",
      activityType: "intermediateMessageCatch",
      processInstanceId: "123456",
      processDefinitionId: "someProcess:00:123456",
      childActivityInstances: [ ],
      childTransitionInstances: [ ],
      executionIds: [
         "123456"
      ],
      activityName: null,
      name: null
    }
  ],
  childTransitionInstances: [ ],
  executionIds: [
    "123456"
  ],
  activityName: "Название бп",
  name: "Название бп"
}
```

Полученная коллекция childActivityInstances содержит необходимый id активной задачи: activityId.