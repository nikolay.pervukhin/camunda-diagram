package net.pervukhin.camundadiagram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamundaDiagramApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamundaDiagramApplication.class, args);
	}

}
