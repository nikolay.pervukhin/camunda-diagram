package net.pervukhin.camundadiagram.controller;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CamundaController {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${camunda.base-url}")
    private String camundaBaseUrl;

    @GetMapping("/process-instance")
    public ArrayNode getProcessInstances(@RequestParam("businessKey") String businessKey) {
        return restTemplate.getForObject(camundaBaseUrl + "/process-instance?businessKey=" + businessKey, ArrayNode.class);
    }

    @GetMapping("/process-instance/{processInstanceId}/activity-instances")
    public ObjectNode getActivityInstance(@PathVariable("processInstanceId") String processInstanceId) {
        return restTemplate.getForObject(camundaBaseUrl + "/process-instance/" +
                processInstanceId + "/activity-instances", ObjectNode.class);
    }

    @GetMapping("/process-definition/{processDefinitionId}/xml")
    public ObjectNode getProcessDefinition(@PathVariable("processDefinitionId") String processDefinitionId) {
        return restTemplate.getForObject(camundaBaseUrl + "/process-definition/" +
                processDefinitionId + "/xml", ObjectNode.class);
    }
}
