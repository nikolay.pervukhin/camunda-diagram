package net.pervukhin.camundadiagram.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DiagramController {

    @GetMapping("/diagram")
    public String greeting(@RequestParam("businessKey") String businessKey, Model model) {
        model.addAttribute("businessKey", businessKey);
        return "diagram-template";
    }
}
